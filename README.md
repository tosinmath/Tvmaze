# Android take home test
Task is to write an Android app, which downloads an episode list from today's US TV broadcasts (http://www.tvmaze.com/api)

## Libraries used

* Google AppCompat Support Libraries
* Google Material Design Support Libraries
* Glide - for image loading
* Rxjava2 - for asynchronous request with observable streams
* Retrofit2 - type-safe REST client to make API request.
* Dagger2 - for dependency injection
* JUnit - for Unit testing
* Mockito - for creating a "mock" for classes
* Espresso - for User Interface testing

## This App has 3 screens
- First screen with content that is loaded dynamically from http://api.tvmaze.com/schedule?country=US&date={current_date}
     1. This screen contains a RecyclerView that shows content in a list with images, and placeholder for those images.
     2. Each row in the list is Clickable and has some text description
     3. On clicking on an Item, it takes you to Episode list Details Activity which In turn leads you to SeriesActivity.

## Other things done.

* The project contain tests Unit Test and UI instrumentation test.
* The design/structure of the application code is production ready and extensible
* This Application is developed entirely in Kotlin
* API 15 as minSdkVersion.
* Fluid layout, that adapts to most phones and tablets.
* All labels, variable names, and identifiers of any kind in English.
* Code is documented.


## Architecture

I implemented Model View Presenter for this project, also known as MVP, the purpose of this is to break the application into modular, single purpose components. Thus encouraging separation of concerns.

This also follows the “SOLID” principles which makes the codebase scalable and more robust.


## Presenter Layer

I implemented 3 Presenter classes (EpisodeListPresenter, DetailPresenter and SeriesPresenter) which sole aim is to be the communicator between the View Layer and Model Layer, acting as a middle man between the two layers.

The 3 Presenters hosts logic to return formatted data from the Model back to the View for display to the user. So each Activity has a matching presenter that handles all access to the model. The presenters passes data to the Activities when the data is ready to display.


## View Layer
The view layer, also known as the UI layer which I named MainActivity, DetailActivity and SeriesActivity in my assignment, It has a reference to the presenter, which is instantiated in the view. The views have interfaces that the presenter interacts with. The main function of the View is to call methods in the Presenter class. The presenters then calls appropriate method in the Views interfaces when data is ready to display.


## Model Layer
The Model Layer, which primarily consist of ( Episode.kt, Show.kt ) and others, They represent the source of the data that I wish to display on the View Layer. It has to do with fetching data from the API, in my case, I implemented **EpisodeInteractor** class for this purpose.

The ultimate goal is to expose an interface that defines the different queries that need to be performed in order to abstract away the type of data source used.

In order for presenters to be served specific related data only.

## Interactors

The interactors will fetch data from my API data source from (http://api.tvmaze.com/schedule?country=US&date={current_date}, http://api.tvmaze.com/episodes/{episode_id} and http://api.tvmaze.com/shows/{show_id} ). After getting the data, the interactor will send the data to the presenter. Thus, making changes in the UI by calling the appropriate method in the Views interfaces when data is ready to display.

## Testing

* This projects has unit tests using JUnit and mockito for mocking objects.
* This project has User Interface test using Espresso testing framework.


I hope to get an opportunity to talk more about my approach and learn ways to implement these more efficiently. Thank you.

Please, see screenshots below:

![](images/screenshot_1.jpg)
![](images/screenshot_2.jpg)
![](images/screenshot_3.jpg)
![](images/screenshot_4.jpg)