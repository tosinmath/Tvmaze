package com.tvmaze

import android.app.Application
import com.tvmaze.di.component.DaggerEpisodeComponent
import com.tvmaze.di.component.DaggerNetComponent
import com.tvmaze.di.component.EpisodeComponent
import com.tvmaze.di.component.NetComponent
import com.tvmaze.di.module.AppModule
import com.tvmaze.di.module.EpisodeModule
import com.tvmaze.di.module.NetModule
import com.tvmaze.di.module.RetrofitModule


class BaseApplication : Application() {

    companion object {
        @JvmStatic lateinit var netComponent: NetComponent
        @JvmStatic lateinit var episodeComponent: EpisodeComponent
    }

    override fun onCreate() {
        super.onCreate()

        netComponent = DaggerNetComponent.builder()
                .appModule(AppModule(this))
                .netModule(NetModule())
                .build()

        episodeComponent = DaggerEpisodeComponent.builder()
                .netComponent(netComponent)
                .retrofitModule(RetrofitModule())
                .episodeModule(EpisodeModule())
                .build()
    }
}