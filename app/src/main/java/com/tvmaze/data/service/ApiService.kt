package com.tvmaze.data.service

import com.tvmaze.BuildConfig
import com.tvmaze.data.model.Episode
import com.tvmaze.data.model.Show
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @GET(BuildConfig.BASE_URL + "schedule?country=US")
    fun getEpisodes(@Query("date") date: String): Observable<List<Episode>>

    @GET(BuildConfig.BASE_URL + "episodes/{episodeId}")
    fun getEpisodeById(@Path("episodeId") episodeId: Int): Observable<Episode>

    @GET(BuildConfig.BASE_URL + "shows/{showId}")
    fun getShowById(@Path("showId") showId: Int): Observable<Show>

}