package com.tvmaze.data.model


import java.util.HashMap

class Image {

    var medium: String? = null
    var original: String? = null

}