package com.tvmaze.data.model


import java.util.HashMap

class Country {

    var name: String? = null
    var code: String? = null
    var timezone: String? = null

}