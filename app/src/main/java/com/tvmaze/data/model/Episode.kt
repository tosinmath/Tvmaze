package com.tvmaze.data.model


class Episode {

    var id: Int? = null
    var url: String? = null
    var name: String? = null
    var season: Int? = null
    var number: Int? = null
    var airdate: String? = null
    var airtime: String? = null
    var airstamp: String? = null
    var runtime: Int? = null
    var image: Image? = null
    var summary: String? = null
    var show: Show? = null
    var links: Links? = null
}