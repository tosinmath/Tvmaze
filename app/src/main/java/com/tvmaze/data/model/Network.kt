package com.tvmaze.data.model


import java.util.HashMap

class Network {

    var id: Int? = null
    var name: String? = null
    var country: Country? = null

}