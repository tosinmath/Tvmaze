package com.tvmaze.data.model


import java.util.HashMap

class Show {

    var id: Int? = null
    var url: String? = null
    var name: String? = null
    var type: String? = null
    var language: String? = null
    var genres: List<String>? = null
    var status: String? = null
    var runtime: Int? = null
    var premiered: String? = null
    var officialSite: String? = null
    var schedule: Schedule? = null
    var rating: Rating? = null
    var weight: Int? = null
    var network: Network? = null
    var webChannel: Any? = null
    var externals: Externals? = null
    var image: Image? = null
    var summary: String? = null
    var updated: Int? = null
    var links: Links? = null

}