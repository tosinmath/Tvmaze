package com.tvmaze.data.interactor

import com.tvmaze.data.model.Episode
import com.tvmaze.data.model.Show
import com.tvmaze.data.service.ApiService
import io.reactivex.Observable


interface EpisodeInteractor {

    fun fetchEpisodes(apiService: ApiService, today: String): Observable<List<Episode>>

    fun fetchEpisodeById(apiService: ApiService, episodeId: Int): Observable<Episode>

    fun fetchShowById(apiService: ApiService, showId: Int): Observable<Show>
}