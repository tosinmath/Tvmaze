package com.tvmaze.data.interactor

import android.app.Application
import com.tvmaze.data.model.Episode
import com.tvmaze.data.model.Show
import com.tvmaze.data.service.ApiService
import io.reactivex.Observable


class EpisodeInteractorImpl() : EpisodeInteractor {

    override fun fetchEpisodes(apiService: ApiService, today: String): Observable<List<Episode>> {
        return apiService.getEpisodes(today)
                .flatMap({ episodes -> Observable.just<List<Episode>>(episodes) })
    }

    override fun fetchEpisodeById(apiService: ApiService, episodeId: Int): Observable<Episode> {
        return apiService.getEpisodeById(episodeId)
                .flatMap({ episodes -> Observable.just<Episode>(episodes) })
    }

    override fun fetchShowById(apiService: ApiService, showId: Int): Observable<Show> {
        return apiService.getShowById(showId)
                .flatMap({ shows -> Observable.just<Show>(shows) })
    }
}