package com.tvmaze.util

import android.content.Context
import android.text.Html
import android.text.Spanned
import android.widget.ImageView
import java.text.SimpleDateFormat


class TextUtil() {
    companion object {
        @SuppressWarnings("deprecation")
        fun fromHtml(html: String): Spanned {
            val result: Spanned
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
            } else {
                result = Html.fromHtml(html)
            }
            return result
        }

        fun formatDate(dateStr: String): String {
            var format = SimpleDateFormat("yyyy-MM-dd")
            var newFormat = SimpleDateFormat("dd-MM-yyyy")
            var date = format.parse(dateStr)
            var str = newFormat.format(date)
            return str
        }
    }
}