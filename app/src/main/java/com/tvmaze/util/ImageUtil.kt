package com.tvmaze.util


import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide


class ImageUtil() {
    companion object {

        fun displayImage(context: Context, imageResource: String, imageView: ImageView, drawable : Int) {
            Glide.with(context)
                    .load(imageResource)
                    .placeholder(drawable)
                    .into(imageView)
        }
    }
}