package com.tvmaze.di.module

import android.app.Application
import com.tvmaze.data.interactor.EpisodeInteractor
import com.tvmaze.data.interactor.EpisodeInteractorImpl
import com.tvmaze.ui.details.DetailPresenter
import com.tvmaze.ui.list.EpisodeListPresenter
import com.tvmaze.ui.series.SeriesPresenter
import dagger.Module
import dagger.Provides


@Module
class EpisodeModule() {

    @Provides
    fun getSeriesPresenter(episodeInteractor: EpisodeInteractor): SeriesPresenter {
        return SeriesPresenter(episodeInteractor)
    }

    @Provides
    fun getDetailPresenter(episodeInteractor: EpisodeInteractor): DetailPresenter {
        return DetailPresenter(episodeInteractor)
    }

    @Provides
    fun getEpisodeListPresenter(episodeInteractor: EpisodeInteractor): EpisodeListPresenter {
        return EpisodeListPresenter(episodeInteractor)
    }

    @Provides
    internal fun provideEpisodeFetcher(): EpisodeInteractor {
        return EpisodeInteractorImpl()
    }

}