package com.tvmaze.di.component

import com.tvmaze.di.module.EpisodeModule
import com.tvmaze.di.module.RetrofitModule
import com.tvmaze.di.scope.UserScope
import com.tvmaze.ui.details.DetailActivity
import com.tvmaze.ui.list.MainActivity
import com.tvmaze.ui.series.SeriesActivity
import dagger.Component


@UserScope
@Component(dependencies = arrayOf(NetComponent::class), modules = arrayOf(RetrofitModule::class, EpisodeModule::class))
interface EpisodeComponent {

    fun inject(mainActivity: MainActivity)
    fun inject(detailActivity: DetailActivity)
    fun inject(seriesActivity: SeriesActivity)

}