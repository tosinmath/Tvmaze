package com.tvmaze.di.component

import android.content.SharedPreferences
import com.tvmaze.di.module.AppModule
import com.tvmaze.di.module.NetModule
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(AppModule::class, NetModule::class))
interface NetComponent {

    // downstream components need these exposed
    fun restAdapter(): Retrofit
    fun sharedPreferences(): SharedPreferences
}