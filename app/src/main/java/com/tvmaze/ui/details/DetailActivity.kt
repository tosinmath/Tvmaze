package com.tvmaze.ui.details

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.tvmaze.BaseApplication
import com.tvmaze.R
import com.tvmaze.data.model.Episode
import com.tvmaze.data.service.ApiService
import com.tvmaze.ui.series.SeriesActivity
import com.tvmaze.util.ImageUtil
import com.tvmaze.util.Logger
import com.tvmaze.util.TextUtil
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class DetailActivity : AppCompatActivity(), DetailView {

    @field:[Inject]
    lateinit var presenter: DetailPresenter

    @field:[Inject]
    lateinit var apiService: ApiService

    private var toolbar: Toolbar? = null
    private var progressBar: ProgressBar? = null
    private var linearLayout: LinearLayout? = null
    private var compositeDisposable: CompositeDisposable? = null
    private var displaySeries: Button? = null;

    private var episodeId: Int? = null
    private var showId: Int? = null

    private var title: TextView? = null
    private var name: TextView? = null
    private var episodeText: TextView? = null
    private var seasonText: TextView? = null
    private var type: TextView? = null
    private var airdate: TextView? = null
    private var airtime: TextView? = null
    private var image: ImageView? = null
    private var summary: TextView? = null
    private var episodeIdText: TextView? = null
    private var runtime: TextView? = null
    private val logger = Logger.getLogger(javaClass)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        BaseApplication.episodeComponent.inject(this)
        presenter.attachView(this)
        compositeDisposable = CompositeDisposable()

        getBundleData()
        setupToolbar()
        initializeViews()
        loadView()
    }

    fun getBundleData() {
        val extras = intent.extras
        if (extras != null) {
            episodeId = extras.getInt(applicationContext.resources.getString(R.string.episode_id))
            showId = extras.getInt(applicationContext.resources.getString(R.string.show_id))
        }
    }

    fun setupToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbar!!.setTitle("")
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    fun initializeViews() {
        displaySeries = findViewById(R.id.display_shows)
        progressBar = findViewById(R.id.progress_bar)
        linearLayout = findViewById(R.id.details_container)
        title = findViewById(R.id.title)
        name = findViewById(R.id.name)
        episodeText = findViewById(R.id.episode)
        seasonText = findViewById(R.id.season_text)
        type = findViewById(R.id.type)
        airdate = findViewById(R.id.airdate)
        airtime = findViewById(R.id.airtime)
        image = findViewById(R.id.image)
        summary = findViewById(R.id.summary)
        episodeIdText = findViewById(R.id.episode_id_text)
        runtime = findViewById(R.id.runtime)
    }

    override fun setEpisodeDetails(episodeDetails: Episode?) {

        hideLoading()
        if (episodeDetails != null) {
            showLayout()

            // set the episode details
            if (episodeDetails.name != null) title!!.setText(episodeDetails.name)

            if (episodeDetails.number != null){
                var episodeStr = applicationContext.getString(R.string.episode_str) + episodeDetails.number
                episodeText!!.setText(episodeStr)
            }
            if (episodeDetails.season != null){
                var seasonStr = applicationContext.getString(R.string.season_str) + episodeDetails.season
                seasonText!!.setText(seasonStr)
            }

            if (episodeDetails.summary != null){
                var summaryStr = TextUtil.fromHtml(episodeDetails.summary.toString())
                summary!!.setText(summaryStr)
            }

            if (episodeDetails.show != null){
                type!!.setText(episodeDetails.show!!.type)
            }

            if (episodeDetails.airdate != null) {
                var airDate = TextUtil.formatDate(episodeDetails.airdate!!)
                airdate!!.setText(airDate)
            }
            if (episodeDetails.airtime != null) airtime!!.setText(episodeDetails.airtime)

            if (episodeDetails.image != null){
                ImageUtil.displayImage(image!!.getContext(),
                        episodeDetails.image!!.original.toString(), image!!, R.drawable.placeholder_original)
            }
            if (episodeDetails.id != null) episodeIdText!!.setText(episodeDetails.id.toString())
            if (episodeDetails.runtime != null) runtime!!.setText(episodeDetails.runtime.toString())

            displaySeries()
        }
    }

    fun loadView() {
        hideLayout()
        if (episodeId !== null) {
            presenter.getEpisodeById(apiService, compositeDisposable!!, episodeId!!)
        }
    }

    fun displaySeries(){
        displaySeries!!.setOnClickListener { v ->
            val context = v.context
            val intent = Intent(context, SeriesActivity::class.java)
            intent.putExtra(context.resources.getString(R.string.show_id), showId)
            val activity = v.context as Activity
            activity.startActivity(intent)
        }
    }

    override fun showLoading() {
        if (progressBar != null && progressBar!!.getVisibility() == View.GONE) {
            progressBar!!.setVisibility(View.VISIBLE)
        }
    }

    override fun hideLoading() {
        if (progressBar != null && progressBar!!.getVisibility() == View.VISIBLE) {
            progressBar!!.setVisibility(View.GONE)
        }
    }

    private fun hideLayout() {
        linearLayout!!.setVisibility(View.GONE)
    }

    private fun showLayout() {
        linearLayout!!.setVisibility(View.VISIBLE)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
        // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable!!.clear()
    }

}
