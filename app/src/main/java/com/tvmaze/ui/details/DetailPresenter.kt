package com.tvmaze.ui.details

import android.app.Application
import com.tvmaze.data.interactor.EpisodeInteractor
import com.tvmaze.data.model.Episode
import com.tvmaze.data.service.ApiService
import com.tvmaze.ui.base.BasePresenter
import com.tvmaze.util.Logger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class DetailPresenter(private val episodeInteractor: EpisodeInteractor) : BasePresenter<DetailView>() {
    private val detailView: DetailView? = null
    private val logger = Logger.getLogger(javaClass)

    override fun attachView(detailView: DetailView) {
        super.attachView(detailView)
    }

    override fun detachView() {
        super.detachView()
    }

    fun getEpisodeById(apiService: ApiService, compositeDisposable: CompositeDisposable, number: Int) {
        if(isViewAttached) {
            mvpView!!.showLoading()
            compositeDisposable.add(episodeInteractor.fetchEpisodeById(apiService, number)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            this::handleResponse,
                            this::handleError
                    ));
        }
    }

    private fun handleResponse(episodes: Episode) {
        mvpView!!.setEpisodeDetails(episodes)
    }

    private fun handleError(error: Throwable) {
        logger.debug(error.localizedMessage)
    }

}