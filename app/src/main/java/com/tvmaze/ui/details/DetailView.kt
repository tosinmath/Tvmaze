package com.tvmaze.ui.details

import com.tvmaze.data.model.Episode
import com.tvmaze.ui.base.MvpView


interface DetailView : MvpView {

    override fun showLoading()

    override fun hideLoading()

    fun setEpisodeDetails(episodeDetails: Episode?)
}