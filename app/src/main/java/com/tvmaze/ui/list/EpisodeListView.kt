package com.tvmaze.ui.list

import com.tvmaze.data.model.Episode
import com.tvmaze.ui.base.MvpView


interface EpisodeListView : MvpView {

    fun setAdapter(episodes: List<Episode>)

    override fun showLoading()

    override fun hideLoading()

    fun loadView()
}