package com.tvmaze.ui.list

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.AbsListView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import com.tvmaze.BaseApplication
import com.tvmaze.R
import com.tvmaze.data.model.Episode
import com.tvmaze.data.service.ApiService
import com.tvmaze.di.component.EpisodeComponent
import com.tvmaze.ui.base.BaseActivity
import com.tvmaze.util.NetworkUtil
import io.reactivex.disposables.CompositeDisposable
import java.util.ArrayList
import javax.inject.Inject

class MainActivity : BaseActivity() , EpisodeListView {


    @field:[Inject]
    lateinit var presenter: EpisodeListPresenter

    @field:[Inject]
    lateinit var apiService: ApiService

    private var progressBar: ProgressBar? = null
    private var compositeDisposable: CompositeDisposable? = null
    private var recyclerView: RecyclerView? = null
    private var layoutManager: LinearLayoutManager? = null
    private var adapter: EpisodeListAdapter? = null


    override fun setupActivity(component: EpisodeComponent?, savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_main)
        BaseApplication.episodeComponent.inject(this)
        presenter.attachView(this)
        compositeDisposable = CompositeDisposable()

        initializeViews()
        loadView()
    }

    override fun setAdapter(episodes: List<Episode>) {
        hideLoading()
        if (episodes.size > 0) {
            val jobsItemList: MutableList<Episode> = ArrayList(episodes)
            adapter = EpisodeListAdapter(applicationContext, jobsItemList)
            recyclerView!!.setAdapter(adapter)
        }
    }

    fun initializeViews(){
        progressBar = findViewById(R.id.progress_bar)
        layoutManager = LinearLayoutManager(applicationContext)
        recyclerView = findViewById(R.id.episodes_recyclerview)
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.setLayoutManager(layoutManager)
    }

    override fun loadView(){
        if (NetworkUtil.isConnected(applicationContext)) {
            populateList()
            hideOfflineSnackBar()
        } else {
            displayOfflineSnackbar()
        }
    }

    fun populateList() {
        presenter.getEpisodeList(apiService, compositeDisposable!!)
    }

    override fun showLoading() {
        progressBar!!.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar!!.visibility = View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable!!.clear()
    }


}
