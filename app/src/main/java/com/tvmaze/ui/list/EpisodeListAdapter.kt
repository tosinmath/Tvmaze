package com.tvmaze.ui.list

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.tvmaze.R
import com.tvmaze.ui.details.DetailActivity
import com.tvmaze.util.ImageUtil
import com.tvmaze.util.Logger
import com.tvmaze.data.model.Episode;


class EpisodeListAdapter(private val context: Context, private val episodes: MutableList<Episode>?) : RecyclerView.Adapter<EpisodeListAdapter.ViewHolder>() {

    private val logger = Logger.getLogger(javaClass)
    private val mTypedValue = TypedValue()
    private val mBackground: Int

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val name: TextView
        val title: TextView
        val type: TextView
        val image: ImageView

        init {
            name = mView.findViewById(R.id.name)
            title = mView.findViewById(R.id.title)
            type = mView.findViewById(R.id.type)
            image = mView.findViewById(R.id.image)
        }

        override fun toString(): String {
            return super.toString()
        }
    }

    fun getValueAt(position: Int): String {
        return episodes!![position].id.toString()
    }

    init {
        context.theme.resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true)
        mBackground = mTypedValue.resourceId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.episode_list, parent, false)
        view.setBackgroundResource(mBackground)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        /* Set your values */
        val model = episodes!![position]

        if (model.show != null) holder.title.setText(model.show!!.name!!)
        if (model.name != null) holder.name.setText(model.name)
        if (model.show!!.type != null && model.airtime != null){
            val typeStr: String = model.show!!.type + " " + context.resources.getString(R.string.type) + " " + model.airtime
            holder.type.setText(typeStr)
        }

        if (model.image != null){
            ImageUtil.displayImage(holder.image.getContext(),
                    model.image!!.medium.toString(), holder.image, R.drawable.placeholder_medium)
        }

        // launch the detail activity to show episodes information
        holder.mView.setOnClickListener { v ->
            val context = v.context
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(context.resources.getString(R.string.episode_id), episodes[holder.adapterPosition].id)
            intent.putExtra(context.resources.getString(R.string.show_id), episodes[holder.adapterPosition].show!!.id)
            val activity = v.context as Activity
            activity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return episodes?.size ?: 0
    }

    fun addAll(data: List<Episode>) {
        episodes!!.addAll(data)
        notifyDataSetChanged()
    }

    fun add(data: Episode) {
        notifyDataSetChanged()
        episodes!!.add(data)
    }

    fun getItemPos(pos: Int): Episode {
        return episodes!![pos]
    }

    fun clear() {
        episodes!!.clear()
    }

}
