package com.tvmaze.ui.list

import android.app.Application
import android.text.format.DateFormat.format
import com.tvmaze.data.interactor.EpisodeInteractor
import com.tvmaze.data.model.Episode
import com.tvmaze.data.service.ApiService
import com.tvmaze.ui.base.BasePresenter
import com.tvmaze.util.Logger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*


class EpisodeListPresenter(private val episodeInteractor: EpisodeInteractor) : BasePresenter<EpisodeListView>() {
    private val episodeListView: EpisodeListView? = null
    private val logger = Logger.getLogger(javaClass)

    override fun attachView(episodeListView: EpisodeListView) {
        super.attachView(episodeListView)
    }

    override fun detachView() {
        super.detachView()
    }

    fun getEpisodeList(apiService: ApiService, compositeDisposable: CompositeDisposable) {
        if(isViewAttached) {
            mvpView!!.showLoading()
            compositeDisposable.add(episodeInteractor.fetchEpisodes(apiService, getTodayDate())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            this::handleResponse,
                            this::handleError
                    ));
        }
    }

    private fun handleResponse(episodes: List<Episode>) {
        mvpView!!.setAdapter(episodes)
    }

    private fun handleError(error: Throwable) {
        logger.debug(error.localizedMessage)
    }

    fun getTodayDate(): String {
        val format = SimpleDateFormat("yyyy-MM-dd")
        val sm = SimpleDateFormat("yyyy-MM-dd")
        val today = StringBuilder()
        val calendar = Calendar.getInstance()
        val month = calendar.get(Calendar.MONTH) + 1;

        today.append(calendar.get(Calendar.YEAR).toString())
        today.append("-")
        today.append(month.toString())
        today.append("-")
        today.append(calendar.get(Calendar.DAY_OF_MONTH).toString())
        val date = format.parse(today.toString())

        return sm.format(date);
    }

}