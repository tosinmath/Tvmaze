package com.tvmaze.ui.base

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import com.tvmaze.R
import com.tvmaze.di.component.EpisodeComponent


abstract class BaseActivity : AppCompatActivity() {

    private val component: EpisodeComponent? = null
    private var snackbarOffline: Snackbar? = null

    protected abstract fun setupActivity(component: EpisodeComponent?, savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActivity(component, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    protected abstract fun loadView()

    fun displayOfflineSnackbar() {
        val snackbarText: TextView
        snackbarOffline = Snackbar.make(findViewById(android.R.id.content), R.string.no_connection_snackbar, Snackbar.LENGTH_INDEFINITE)
        snackbarText = snackbarOffline!!.getView().findViewById(android.support.design.R.id.snackbar_text)
        snackbarText.setTextColor(resources.getColor(R.color.colorPrimary))
        snackbarOffline!!.setAction(R.string.snackbar_action_retry, View.OnClickListener { loadView() })
        snackbarOffline!!.setActionTextColor(resources.getColor(R.color.colorPrimary))
        snackbarOffline!!.show()
    }

    fun hideOfflineSnackBar() {
        if (snackbarOffline != null && snackbarOffline!!.isShown()) {
            snackbarOffline!!.dismiss()
        }
    }


}