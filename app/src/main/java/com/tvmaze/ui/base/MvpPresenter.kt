package com.tvmaze.ui.base


interface MvpPresenter<V : MvpView> {

    fun attachView(mvpView: V)

    fun detachView()
}