package com.tvmaze.ui.base


interface MvpView {

    fun showLoading()

    fun hideLoading()

}