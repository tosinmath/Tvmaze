package com.tvmaze.ui.series

import com.tvmaze.data.model.Episode
import com.tvmaze.data.model.Show
import com.tvmaze.ui.base.MvpView


interface SeriesView : MvpView {

    override fun showLoading()

    override fun hideLoading()

    fun setSeriesDetails(seriesDetails: Show?)
}