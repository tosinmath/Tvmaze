package com.tvmaze.ui.series

import com.tvmaze.data.interactor.EpisodeInteractor
import com.tvmaze.data.model.Episode
import com.tvmaze.data.model.Show
import com.tvmaze.data.service.ApiService
import com.tvmaze.ui.base.BasePresenter
import com.tvmaze.ui.details.DetailView
import com.tvmaze.util.Logger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class SeriesPresenter(private val episodeInteractor: EpisodeInteractor) : BasePresenter<SeriesView>() {
    private val seriesView: SeriesView? = null
    private val logger = Logger.getLogger(javaClass)

    override fun attachView(seriesView: SeriesView) {
        super.attachView(seriesView)
    }

    override fun detachView() {
        super.detachView()
    }

    fun getSeriesById(apiService: ApiService, compositeDisposable: CompositeDisposable, seriesId: Int) {
        if(isViewAttached) {
            mvpView!!.showLoading()
            compositeDisposable.add(episodeInteractor.fetchShowById(apiService, seriesId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            this::handleResponse,
                            this::handleError
                    ));
        }
    }

    private fun handleResponse(series: Show) {
        mvpView!!.setSeriesDetails(series)
    }

    private fun handleError(error: Throwable) {
        logger.debug(error.localizedMessage)
    }

}