package com.tvmaze.ui.series

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.Html
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.tvmaze.BaseApplication
import com.tvmaze.R
import com.tvmaze.data.model.*
import com.tvmaze.data.service.ApiService
import com.tvmaze.ui.details.DetailPresenter
import com.tvmaze.ui.details.DetailView
import com.tvmaze.util.ImageUtil
import com.tvmaze.util.Logger
import com.tvmaze.util.TextUtil
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SeriesActivity : AppCompatActivity(), SeriesView {

    @field:[Inject]
    lateinit var presenter: SeriesPresenter

    @field:[Inject]
    lateinit var apiService: ApiService

    private var toolbar: Toolbar? = null
    private var progressBar: ProgressBar? = null
    private var linearLayout: LinearLayout? = null
    private var compositeDisposable: CompositeDisposable? = null

    private var showId: Int? = null

    private val logger = Logger.getLogger(javaClass)
    private var name: TextView? = null
    private var type: TextView? = null
    private var premiered: TextView? = null
    private var rating: TextView? = null
    private var network: TextView? = null
    private var image: ImageView? = null
    private var summary: TextView? = null
    private var detailTitle: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_series)
        BaseApplication.episodeComponent.inject(this)
        presenter.attachView(this)
        compositeDisposable = CompositeDisposable()

        getBundleData()
        setupToolbar()
        initializeViews()
        loadView()
    }

    fun getBundleData() {
        val extras = intent.extras
        if (extras != null) {
            showId = extras.getInt(applicationContext.resources.getString(R.string.show_id))
        }
    }

    fun setupToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbar!!.setTitle("")
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    fun initializeViews() {
        progressBar = findViewById(R.id.progress_bar)
        linearLayout = findViewById(R.id.details_container)
        detailTitle = findViewById(R.id.detail_title)
        name = findViewById(R.id.name)
        type = findViewById(R.id.type)
        premiered = findViewById(R.id.premiered)
        rating = findViewById(R.id.rating)
        network = findViewById(R.id.network)
        image = findViewById(R.id.image)
        summary = findViewById(R.id.summary)
    }

    override fun setSeriesDetails(seriesDetails: Show?) {

        hideLoading()
        if (seriesDetails != null) {
            showLayout()

            // set the series details
            if (seriesDetails.name != null) name!!.setText(seriesDetails.name)
            if (seriesDetails.type != null) type!!.setText(seriesDetails.type.toString())
            if (seriesDetails.premiered != null) {
                var premieredDate = TextUtil.formatDate(seriesDetails.premiered!!)
                premiered!!.setText(premieredDate)
            }
            if (seriesDetails.rating != null) rating!!.setText(seriesDetails.rating!!.average.toString())
            if (seriesDetails.network != null && seriesDetails.network!!.name != null){
                network!!.setText(seriesDetails.network!!.name)
            }

            if (seriesDetails.summary != null){
                val summaryStr = TextUtil.fromHtml(seriesDetails.summary.toString())
                summary!!.setText(summaryStr)
            }
            if (seriesDetails.image != null){
                ImageUtil.displayImage(image!!.getContext(),
                        seriesDetails.image!!.original.toString(), image!!, R.drawable.placeholder_show)
            }
            if (seriesDetails.name != null) detailTitle!!.setText(seriesDetails.name)
        }
    }

    fun loadView() {
        hideLayout()
        if (showId !== null) {
            presenter.getSeriesById(apiService, compositeDisposable!!, showId!!)
        }
    }

    override fun showLoading() {
        if (progressBar != null && progressBar!!.getVisibility() == View.GONE) {
            progressBar!!.setVisibility(View.VISIBLE)
        }
    }

    override fun hideLoading() {
        if (progressBar != null && progressBar!!.getVisibility() == View.VISIBLE) {
            progressBar!!.setVisibility(View.GONE)
        }
    }

    private fun hideLayout() {
        linearLayout!!.setVisibility(View.GONE)
    }

    private fun showLayout() {
        linearLayout!!.setVisibility(View.VISIBLE)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
        // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable!!.clear()
    }


}
