package com.tvmaze.ui.list


import android.support.test.espresso.ViewInteraction
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.test.suitebuilder.annotation.LargeTest
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.EditText
import android.widget.TextView

import com.tvmaze.R

import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

import java.util.concurrent.ExecutionException

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withClassName
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.`is`

@LargeTest
@RunWith(AndroidJUnit4::class)
class EpisodeListUITest {

    // This tests checks if the name TextView value (on the Episode List RecyclerView) is Equal to DetailActivity title.
    // This checks the accuracy of data If/When user clicks on an Item on the List

    var recyclerView: RecyclerView? = null

    @Rule @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    @Throws(InterruptedException::class, ExecutionException::class)
    fun episodeListUITest() {

        Thread.sleep(4000)

        recyclerView = mActivityTestRule.activity.findViewById(R.id.episodes_recyclerview)
        val viewHolder = recyclerView!!.findViewHolderForAdapterPosition(0)
        val titleOnList = (viewHolder.itemView.findViewById<View>(R.id.name) as TextView).text.toString()

        val recyclerView = onView(
                allOf(withId(R.id.episodes_recyclerview),
                        childAtPosition(
                                withClassName(`is`("android.widget.LinearLayout")),
                                1)))
        recyclerView.perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))


        // Perform the check
        onView(withId(R.id.title))
                .check(matches(hasValueEqualTo(titleOnList)))
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return (parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position))
            }
        }
    }

    internal fun hasValueEqualTo(content: String): Matcher<View> {

        return object : TypeSafeMatcher<View>() {

            override fun describeTo(description: Description) {
                description.appendText("Has EditText/TextView the value:  " + content)
            }

            public override fun matchesSafely(view: View?): Boolean {
                if (view !is TextView && view !is EditText) {
                    return false
                }
                if (view != null) {
                    val text: String
                    if (view is TextView) {
                        text = view.text.toString()
                    } else {
                        text = (view as EditText).text.toString()
                    }

                    return text.equals(content, ignoreCase = true)
                }
                return false
            }
        }
    }
}
