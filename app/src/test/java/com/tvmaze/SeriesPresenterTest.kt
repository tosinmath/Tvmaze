package com.tvmaze

import com.tvmaze.data.interactor.EpisodeInteractor
import com.tvmaze.data.model.*
import com.tvmaze.data.service.ApiService
import com.tvmaze.ui.series.SeriesPresenter
import com.tvmaze.ui.details.DetailView
import com.tvmaze.ui.series.SeriesView
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

class SeriesPresenterTest {

    // This presenter tests if the Expected methods are called when getSeriesById is being initiated.
    // The 2 methods are setSeriesDetails(seriesDetails: Show?) and showLoading()

    @Mock
    internal var apiService: ApiService? = null

    @Mock
    internal var episodeInteractor: EpisodeInteractor? = null

    @Mock
    internal var seriesView: SeriesView? = null

    private var presenter: SeriesPresenter? = null

    var compositeDisposable: CompositeDisposable? = null
    internal var seriesId = 9

    var show = showData();

    companion object {

        @BeforeClass
        fun setUpRxSchedulers() {
            val immediate = object : Scheduler() {
                override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                    return super.scheduleDirect(run, 0, unit)
                }

                override fun createWorker(): Worker {
                    return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
                }
            }

            RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        }
    }

    @Before
    @Throws(Exception::class)
    fun setUp() {
        setUpRxSchedulers()
        MockitoAnnotations.initMocks(this)

        compositeDisposable = CompositeDisposable()
        Mockito.`when`(episodeInteractor!!.fetchShowById(apiService!!, seriesId)).thenReturn(Observable.just<Show>(show))

        presenter = SeriesPresenter(episodeInteractor!!)
        presenter!!.attachView(seriesView!!)
    }

    @Test
    @Throws(Exception::class)
    fun testSeriesPresenter() {
        presenter!!.getSeriesById(apiService!!, compositeDisposable!!, seriesId)
        Mockito.verify(seriesView)!!.setSeriesDetails(show)
        Mockito.verify(seriesView)!!.showLoading()
    }

    private fun showData(): Show {

        // Setting up the values for test
        val show = Show()

        show.id = 2831
        show.url = "http://www.tvmaze.com/shows/2831/the-late-late-show-with-james-corden"
        show.name = "The Late Late Show with James Corden"
        show.type = "Talk Show"
        show.language = "English"

        val genreList = listOf("Comedy", "Music")
        show.genres = genreList

        var status = "Running"
        var runtime = 60
        var premiered = "2015-03-23"
        var officialSite = "http://www.cbs.com/shows/late-late-show/"
        var schedule = null
        var rating = null
        var weight = 94
        var image: Image? = null
        var summary = "\"<p>Each week night, <b>The Late Late Show with James Corden </b>throws the ultimate late night after part"
        var updated = 1519824782

        return show;
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        presenter!!.detachView()
    }



}