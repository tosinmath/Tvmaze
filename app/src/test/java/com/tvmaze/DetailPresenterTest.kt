package com.tvmaze

import com.tvmaze.data.interactor.EpisodeInteractor
import com.tvmaze.data.model.*
import com.tvmaze.data.service.ApiService
import com.tvmaze.ui.details.DetailPresenter
import com.tvmaze.ui.details.DetailView
import com.tvmaze.ui.list.EpisodeListPresenter
import com.tvmaze.ui.list.EpisodeListView
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.ArrayList
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

class DetailPresenterTest {

    // This presenter tests if the Expected methods are called when getEpisodeById is being initiated.
    // The 2 methods are setEpisodeDetails(episodeDetails: Episode?) and showLoading()

    @Mock
    internal var apiService: ApiService? = null

    @Mock
    internal var episodeInteractor: EpisodeInteractor? = null

    @Mock
    internal var detailView: DetailView? = null

    private var presenter: DetailPresenter? = null

    var compositeDisposable: CompositeDisposable? = null
    internal var number = 1

    var episodes = episodeData();

    companion object {

        @BeforeClass
        fun setUpRxSchedulers() {
            val immediate = object : Scheduler() {
                override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                    return super.scheduleDirect(run, 0, unit)
                }

                override fun createWorker(): Worker {
                    return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
                }
            }

            RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        }
    }

    @Before
    @Throws(Exception::class)
    fun setUp() {
        setUpRxSchedulers()
        MockitoAnnotations.initMocks(this)

        compositeDisposable = CompositeDisposable()
        Mockito.`when`(episodeInteractor!!.fetchEpisodeById(apiService!!, number)).thenReturn(Observable.just<Episode>(episodes))

        presenter = DetailPresenter(episodeInteractor!!)
        presenter!!.attachView(detailView!!)
    }

    @Test
    @Throws(Exception::class)
    fun testDetailPresenter() {
        presenter!!.getEpisodeById(apiService!!, compositeDisposable!!, number)
        Mockito.verify(detailView)!!.setEpisodeDetails(episodes)
        Mockito.verify(detailView)!!.showLoading()
    }

    private fun episodeData(): Episode {

        // Setting up the values for test
        val episode = Episode()
        episode.id = 1
        episode.url = "http://www.tvmaze.com/episodes/1/under-the-dome-1x01-pilot"
        episode.name  = "Pilot"
        episode.season  = 1
        episode.number = 1
        episode.airdate = "2013-06-24"
        episode.airtime = "22:00"
        episode.airstamp  = "2013-06-25T02:00:00+00:00"
        episode.runtime  = 60
        episode.image = null
        episode.summary = "<p>When the residents of Chester's Mill find themselves trapped under a massive transparent dome with no way out</p>"
        episode.links = null

        return episode;
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        presenter!!.detachView()
    }



}