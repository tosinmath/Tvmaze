package com.tvmaze

import android.app.Application
import com.tvmaze.data.interactor.EpisodeInteractor
import com.tvmaze.data.model.Episode
import com.tvmaze.data.service.ApiService
import com.tvmaze.ui.list.EpisodeListPresenter
import com.tvmaze.ui.list.EpisodeListView
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit


class EpisodePresenterTest {

    // This presenter tests if the Expected methods are called when getEpisodeList is being initiated.
    // The 2 methods are setAdapter(episodes: List<Episode>) and showLoading()

    @Mock
    internal var apiService: ApiService? = null

    @Mock
    internal var episodeInteractor: EpisodeInteractor? = null

    @Mock
    internal var episodeListView: EpisodeListView? = null

    private var presenter: EpisodeListPresenter? = null

    var compositeDisposable: CompositeDisposable? = null
    internal var today = "2018-03-01"

    companion object {

        @BeforeClass
        fun setUpRxSchedulers() {
            val immediate = object : Scheduler() {
                override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                    return super.scheduleDirect(run, 0, unit)
                }

                override fun createWorker(): Scheduler.Worker {
                    return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
                }
            }

            RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        }
    }

    @Before
    @Throws(Exception::class)
    fun setUp() {
        setUpRxSchedulers()
        MockitoAnnotations.initMocks(this)

        compositeDisposable = CompositeDisposable()

        val episodes = mutableListOf<Episode>()
        `when`(episodeInteractor!!.fetchEpisodes(apiService!!, today)).thenReturn(Observable.just<List<Episode>>(episodes))

        presenter = EpisodeListPresenter(episodeInteractor!!)
        presenter!!.attachView(episodeListView!!)
    }

    @Test
    @Throws(Exception::class)
    fun testEpisodePresenter() {
        presenter!!.getEpisodeList(apiService!!, compositeDisposable!!)
        val episodes = mutableListOf<Episode>()
        verify(episodeListView)!!.setAdapter(episodes)
        verify(episodeListView)!!.showLoading()
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        presenter!!.detachView()
    }



}